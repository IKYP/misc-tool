#include <stdio.h>
#include <stdlib.h>

static float bmi(const float weight, const float tall)
{
        return weight / (tall * tall);
}

int main(int argc, char **argv)
{
	float tall, weight;
	
	if (argc != 3) {
		fputs("Usage: bmi Tall(M) Weight(Kg)\n", stdout);
		return EXIT_FAILURE;
	}

	tall   = atof(argv[1]);
	weight = atof(argv[2]);

	fprintf(stdout, "Tall(M)   : %.2lfM\n\
Weight(kg): %.2lfKg\n\
BMI       : %.2lf%%\n", tall, weight, bmi(weight, tall));

	return EXIT_SUCCESS;
}