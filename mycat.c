#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>

#define LINE_BUFF 1024
#define B_NOR_BLUE  "\e[0;44m"
#define F_BLOD_CYAN "\e[1;36m"
#define TAG_END     "\e[m"

bool is_directory(const char * path)
{
        struct stat file_status;
        if (stat(path, &file_status) < 0) {
                perror("WRONG");
                exit(EXIT_FAILURE);
        }

        return S_ISDIR(file_status.st_mode) ? true : false;
}

int main(int argc, char **argv)
{
        static char line[LINE_BUFF];
        static int line_num = 1;
        FILE *fd;
        
        if (argc != 2) {
                fputs("Usage: mycat FILE_NAME\n", stderr);
                exit(EXIT_FAILURE);
        }

        if (is_directory(argv[1])) {
                fprintf(stderr, "%s is a directory. Not a file. Can't show with mycat as a file.\n", argv[1]);
                exit(EXIT_FAILURE);
        }

        if ((fd = fopen(argv[1], "r")) == NULL) {
                perror("WRONG");
                exit(EXIT_FAILURE);
        }

        fputs("\e[1;42m################################### Beginning ##################################\e[m\n", stdout);

        while((fgets(line, LINE_BUFF, fd)) != NULL) {
                fprintf(stdout, "%s%-4d%s %s%s%s",
                        B_NOR_BLUE,  line_num, TAG_END,
                        F_BLOD_CYAN, line,     TAG_END);
                ++line_num;
        }
        
        fputs("\n\e[1;42m##################################### End ######################################\e[m\n", stdout);
        fclose(fd);

        return EXIT_SUCCESS;
}