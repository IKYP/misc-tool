#include <stdio.h>
#include <stdlib.h>
#include <sys/utsname.h>

int main(void)
{
        static struct utsname uts;
  
        if (uname(&uts) == -1) {
                perror("Uname Function Error: ");
                return EXIT_FAILURE;
        }

        fprintf(stdout,
      	        "Host   Name: %s\n"
	        "System Name: %s\n"
	        "Release    : %s\n"
	        "Version    : %s\n"
	        "Platform   : %s\n",
	        uts.nodename, uts.sysname,
	        uts.release,  uts.version,
	        uts.machine);

        return EXIT_SUCCESS;
}
