#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SIZE 33

int main(void)
{
	static time_t t;
	static struct tm *now;
	static char buf[SIZE];

	t   = time(NULL);
	now = localtime(&t);
	if (now == NULL) {
		fputs("Localtime Function Error!\n", stdout);
		exit(EXIT_FAILURE);
	}

	if (strftime(buf, SIZE, "%F %A %I:%M:%S %p", now) == 0) {
		fputs("Strftime Function Error!\n", stdout);
		exit(EXIT_FAILURE);
	}

	fprintf(stdout, "%s\n", buf);
	return EXIT_SUCCESS;
}
