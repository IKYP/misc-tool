#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static const float f2c(const float temp_f)
{
	return ((temp_f - 32) / 1.8);
}

static const float c2f(const float temp_c)
{
	return (temp_c * 1.8 + 32);
}

static void usage(void)
{
	fprintf(stdout, "\
tempconvert        -- A tool for convert temperture with Celsius and Fahrenheit\n\
Usage: tempconvert [OPTION] NUMBER\n\
       OPTION: f|c\n\
	       f  Convert temperture from Fahrenheit to Celsius\n\
	       c  Convert temperture from Celsius to  Fahrenheit\n\
       NUMBER: temperture number\n\
Example: tempconvert f 80\n");
}

int main(int argc, char **argv)
{
	if (argc != 3) {
		usage();
		return EXIT_FAILURE;
	}
	
	if (strcmp(argv[1], "f") == 0) {
		fprintf(stdout, "After convert temperture from Fahrenheit to Celsius is %.2f°C\n",
			f2c(atof(argv[2])));
	} else if (strcmp(argv[1], "c") == 0) {
		fprintf(stdout, "After convert temperture from Celsius to Fahrenheit is %.2f°F\n",
			c2f(atof(argv[2])));
	} else {
		fputs("Unknow arguments\n", stdout);
		usage();
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
