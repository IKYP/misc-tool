#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#define CYAN    "\e[1;36m"
#define TAG_END "\e[m"

int main(int argc, char **argv)
{
        int num;
        srand((unsigned)time(NULL));
        
	int length = 100;
        
	if (argc == 2) {
		length = atoi(argv[1]);
	}
        
        for (int i = 1; i <= length; ++i) {
                num = rand() % 10;
	        fprintf(stdout, "%s%-2d%s", CYAN, num, TAG_END);

                if (i % 10 == 0) {
			fputs("\n", stdout);
		}
	}

        fputs("\n", stdout);
        return EXIT_SUCCESS;
}